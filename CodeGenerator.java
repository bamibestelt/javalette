import Javalette.Absyn.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Void;
import java.util.*;

/**
 * Created by bamibestelt on 2017-04-24.
 */
public class CodeGenerator {

    private LinkedList<String> output;

    private int address = 0;
    private final String VAR_SIGN = "%";
    private Map<String,Func> sig;


    private List<Map<String,Type>> varsType;
    private List<Map<String,String>> registerTable;


    private String currentPointer;
    private Type currentPointerType;


    private List<String> booleanStack;
    private int stackCounter = 0;
    private String BOOL = VAR_SIGN + "bool_";


    private int labelCounter = 0;
    private int blockLevel = 0;
    private int stringCounter = 0;
    private LinkedList<String> stringDeclaration;


    private String OBJECT = VAR_SIGN + "object";
    private final int SIZE = 10;
    private int size = 3;
    private int nitems = SIZE * size * size;


    // tabs for aligning the llvm code
    private final String TAB = "\t";
    private final String DOUBLE_TAB = "\t\t";


    private boolean isWithLabel = false;
    private boolean anyArray = false;
    private Stmt theReturn = null;



    public void generate(String absolutePath, Prog p) {
        String path = absolutePath.substring(0, absolutePath.indexOf(".jl"));
        String name = FilenameUtils.getBaseName(absolutePath);
        name = name.substring(0, name.indexOf(".jl"));
        output = new LinkedList<>();

        sig = new TreeMap<>();
        sig = PrimitiveFunctions.addRuntimeToSignature(sig);

        for (TopDef d: ((Program)p).listtopdef_) {
            FnDef def = (FnDef)d;
            sig.put(def.ident_, new Func(def.ident_, def.type_, def.listarg_));
        }

        p.accept(new ProgramVisitor(), null);
        String runtime = PrimitiveFunctions.runtimeDeclaration();
        output.add(0, runtime);

        try {
            String runtimeCode = "lib/runtime.bc";
            //String runtimeCode = "runtime.bc";
            PrintWriter out = new PrintWriter(path+".ll");
            for (String s: output) {
                System.out.print(s);
                out.print(s);
            }
            out.close();

            // run command to translate .ll to .bc
            Process toBC = java.lang.Runtime.getRuntime().exec("llvm-as "+path+".ll");
            toBC.waitFor();
            Process linking = java.lang.Runtime.getRuntime().exec("llvm-link "+path+".bc "
                    +runtimeCode+" -o "+ name +".bc");
            linking.waitFor();
            Process toObject = java.lang.Runtime.getRuntime().exec("llc -filetype=obj "
                    + name +".bc");
            toObject.waitFor();
            Process toOut = java.lang.Runtime.getRuntime().exec("gcc "+ name +".o -o "
                    + name +".out");
            toOut.waitFor();
            Process rm = java.lang.Runtime.getRuntime().exec("rm -rf "
                    + name +".o "+ name +".out");
            rm.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }



    /** Program */
    private class ProgramVisitor implements Program.Visitor<Void,Void> {

        @Override
        public Void visit(Program p, Void arg) {
            stringDeclaration = new LinkedList<>();
            for(TopDef def : p.listtopdef_) {
                theReturn = null;
                def.accept(new DefVisitor(), null);
            }
            output.addAll(0, stringDeclaration);
            addArrayObject();
            return null;
        }
    }



    /** Functions */
    private class DefVisitor implements TopDef.Visitor<Void,Void> {

        @Override
        public Void visit(FnDef p, Void arg) {
            registerTable = new LinkedList<>();
            registerTable.add(new TreeMap<>());
            varsType = new LinkedList<>();
            varsType.add(new TreeMap<>());
            booleanStack = new ArrayList<>();

            output.add("\n" + sig.get(p.ident_).toLLVM() + "\n");
            emit(new Entry());

            int index = 0;
            for (Arg x: p.listarg_) {
                x.accept(new ArgsVisitor(index), null);
                index++;
            }

            Block block = (Block) p.blk_;
            for (Stmt s: block.liststmt_) {
                s.accept(new StmtVisitor(), null);
            }

            functionEnding(p.type_);
            output.add("}\n");
            return null;
        }
    }



    /** Statements */
    private class StmtVisitor implements Stmt.Visitor<Void,Void> {

        @Override
        public Void visit(Empty p, Void arg) {
            return null;
        }

        @Override
        public Void visit(BStmt p, Void arg) {
            Block block = (Block) p.blk_;
            addBlockLevel();
            for(Stmt stmt : block.liststmt_) {
                stmt.accept(new StmtVisitor(), arg);
            }
            removeTopBlock();
            return null;
        }

        @Override
        public Void visit(Decl p, Void arg) {
            for(Item item: p.listitem_) {
                item.accept(new ItemVisitor(p.type_), arg);
            }
            return null;
        }

        @Override
        public Void visit(Incr p, Void arg) {
            Type type = lookupType(lookupAddress(p.ident_));

            String source = getPointer(type);
            emit(new Load(type, source, lookupAddress(p.ident_)));
            String target = getPointer(type);
            emit(new Add(type, source, "1", target));
            emit(new Store(type, target, lookupAddress(p.ident_)));
            return null;
        }

        @Override
        public Void visit(Decr p, Void arg) {
            Type type = lookupType(lookupAddress(p.ident_));

            String source = getPointer(type);
            emit(new Load(type, source, lookupAddress(p.ident_)));
            String target = getPointer(type);
            emit(new Sub(type, source, "1", target));
            emit(new Store(type, target, lookupAddress(p.ident_)));
            return null;
        }

        @Override
        public Void visit(Ret p, Void arg) {
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_.accept(new ExprVisitor(), arg);
            if(getBasic(currentPointerType) instanceof Bool) {topStackEval();
            } else {topStackRemove();}
            if(!PrimitiveFunctions.isThisArray(currentPointerType)) {
                emit(new Return(currentPointerType, currentPointer));
            } else {
                emit(new ReturnObj(OBJECT, currentPointer));
            }
            theReturn = p;
            return null;
        }

        @Override
        public Void visit(VRet p, Void arg) {
            emit(new Return(new TBasic(new Javalette.Absyn.Void()), null));
            theReturn = p;
            return null;
        }

        @Override
        public Void visit(Cond p, Void arg) {
            // make new boolean on the stack
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);

            // evaluate expression and store the final value into pointer
            p.expr_.accept(new ExprVisitor(), arg);

            // construct labels for all conditions and go out
            Label stmTrue = makeLabel();
            Label stmFalse = makeLabel();
            Label stmOut = makeLabel();

            // evaluate the top of the stack
            topStackEval();
            // emit conditional branch
            emit(new Branch(currentPointer, stmTrue, stmFalse));

            // emit a label if condition is true
            emit(stmTrue);
            p.stmt_.accept(new StmtVisitor(), arg);
            emit(new Branch(null, stmOut, null));

            // if condition is false
            emit(stmFalse);
            emit(new Branch(null, stmOut, null));

            emit(stmOut);
            return null;
        }

        @Override
        public Void visit(CondElse p, Void arg) {
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_.accept(new ExprVisitor(), arg);

            Label stmTrue = makeLabel();
            Label stmFalse = makeLabel();
            Label stmOut = makeLabel();

            topStackEval();
            emit(new Branch(currentPointer, stmTrue, stmFalse));

            emit(stmTrue);
            p.stmt_1.accept(new StmtVisitor(), arg);
            emit(new Branch(null, stmOut, null));

            emit(stmFalse);
            p.stmt_2.accept(new StmtVisitor(), arg);
            emit(new Branch(null, stmOut, null));

            emit(stmOut);
            return null;
        }

        @Override
        public Void visit(While p, Void arg) {
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);

            Label stmCondition = makeLabel();
            emit(new Branch(null, stmCondition, null));

            emit(stmCondition);
            p.expr_.accept(new ExprVisitor(), arg);

            Label stmTrue = makeLabel();
            Label stmFalse = makeLabel();

            topStackEval();
            emit(new Branch(currentPointer, stmTrue, stmFalse));

            emit(stmTrue);
            p.stmt_.accept(new StmtVisitor(), arg);
            emit(new Branch(null, stmCondition, null));

            emit(stmFalse);
            return null;
        }

        @Override
        public Void visit(ForEach p, Void arg) {
            Type typeInt = new TBasic(new Int());
            String name = lookupAddress(p.ident_2);
            Type type = lookupType(name);

            // make local index variable
            newVar("index", typeInt, false);
            String index = lookupAddress("index");

            // branch to condition
            Label condition = makeLabel();
            emit(new Branch(null, condition, null));
            emit(condition);
            ERel eRel = new ERel(new EVar("index"), new LTH(), new EArrLen(p.ident_2));
            eRel.accept(new ExprVisitor(), arg);

            Label stmTrue = makeLabel();
            Label stmFalse = makeLabel();
            emit(new Branch(currentPointer, stmTrue, stmFalse));

            emit(stmTrue);
            addBlockLevel();
            newVar(p.ident_1, p.type_, false);
            Expr eArrLit = new EArrLit(p.ident_2, new EVar("index"));
            eArrLit.accept(new ExprVisitor(), arg);
            emit(new Store(type, currentPointer, lookupAddress(p.ident_1)));
            p.stmt_.accept(new StmtVisitor(), arg);
            removeTopBlock();
            blockLevel++;

            Expr eAdd = new EAdd(new EVar("index"), new Plus(), new ELitInt(1));
            eAdd.accept(new ExprVisitor(), arg);
            emit(new Store(typeInt, currentPointer, index));
            emit(new Branch(null, condition, null));
            emit(stmFalse);
            return null;
        }

        @Override
        public Void visit(SExp p, Void arg) {
            p.expr_.accept(new ExprVisitor(), arg);
            return null;
        }
    }



    /** Expressions */
    private class ExprVisitor implements Expr.Visitor<Void,Void> {

        @Override
        public Void visit(EVar p, Void arg) {
            String name = lookupAddress(p.ident_);
            Type type = lookupType(name);
            String addrs = getPointer(type);
            if(!PrimitiveFunctions.isThisArray(type)) {
                emit(new Load(type, addrs, name));
            } else {
                currentPointer = name;
                currentPointerType = type;
            }
            if(getBasic(currentPointerType) instanceof Bool)
                topStackAssign(addrs);
            return null;
        }

        @Override
        public Void visit(ELitInt p, Void arg) {
            currentPointer = String.valueOf(p.integer_);
            currentPointerType = new TBasic(new Int());
            return null;
        }

        @Override
        public Void visit(ELitDoub p, Void arg) {
            currentPointer = String.valueOf(p.double_);
            currentPointerType = new TBasic(new Doub());
            return null;
        }

        @Override
        public Void visit(ELitTrue p, Void arg) {
            currentPointer = String.valueOf("1");
            currentPointerType = new TBasic(new Bool());
            topStackAssign("1");
            return null;
        }

        @Override
        public Void visit(ELitFalse p, Void arg) {
            currentPointer = String.valueOf("0");
            currentPointerType = new TBasic(new Bool());
            topStackAssign("0");
            return null;
        }

        @Override
        public Void visit(EApp p, Void arg) {
            String immed = null;
            Func func = sig.get(p.ident_);
            Type fType = func.getFunctionType();
            int index = 0;

            LinkedList<String> values = new LinkedList<>();
            for (Expr e: p.listexpr_) {
                Argument argument = (Argument) func.getFunctionArgs().get(index);
                boolean isBool = getBasic(argument.type_) instanceof Bool;
                if(isBool)newVar(BOOL+stackCounter, new TBasic(new Bool()), true);

                e.accept(new ExprVisitor(), arg);
                if(isBool)topStackEval();

                values.add(currentPointer);
                index++;
            }
            if(!(getBasic(fType) instanceof Javalette.Absyn.Void))immed = getPointer(fType);
            emit(new Call(func, immed, values));
            if(getBasic(fType) instanceof Bool)topStackAssign(immed);
            return null;
        }

        @Override
        public Void visit(EString p, Void arg) {
            String label = "@string_" + stringCounter;
            Code code = new StringArr(label, p.string_.length(), p.string_, null);
            String str_llvm = code.accept(new CodeToJVM());

            String pointer = getPointer(new TBasic(new Str()));
            emit(new StringArr(label, p.string_.length(), p.string_, pointer));
            stringDeclaration.add(str_llvm);
            stringCounter++;
            return null;
        }

        @Override
        public Void visit(EArrLit p, Void arg) {
            p.expr_.accept(new ExprVisitor(), arg);
            String index = currentPointer;

            String var = lookupAddress(p.ident_);
            Type type = new TBasic(getBasic(lookupType(var)));
            String immed_1 = getPointer(type);
            emit(new ArrElm(OBJECT, immed_1, var, index, type));
            String immed_2 = getPointer(type);
            emit(new Load(type, immed_2, immed_1));
            return null;
        }

        @Override
        public Void visit(EArrLen p, Void arg) {
            Type tInt = new TBasic(new Int());
            String immed_1 = getPointer(tInt);
            emit(new ArrLen(OBJECT, immed_1, lookupAddress(p.ident_)));
            String immed_2 = getPointer(tInt);
            emit(new Load(tInt, immed_2, immed_1));
            return null;
        }

        @Override
        public Void visit(ENew p, Void arg) {
            for(DimExpr dimExpr : p.listdimexpr_) {
                DExpr dExpr = (DExpr) dimExpr;
                dExpr.expr_.accept(new ExprVisitor(), arg);
            }
            return null;
        }

        @Override
        public Void visit(Neg p, Void arg) {
            String immed_1 = "0";
            p.expr_.accept(new ExprVisitor(), arg);
            String immed_2 = currentPointer;
            Type type = currentPointerType;

            if(getBasic(type) instanceof Doub)
                immed_1 = "0.0";

            String immed_3 = getPointer(type);
            emit(new Sub(type, immed_1, immed_2, immed_3));
            return null;
        }

        @Override
        public Void visit(Not p, Void arg) {
            p.expr_.accept(new ExprVisitor(), arg);
            String immed_1 = currentPointer;
            String immed_2 = getPointer(new TBasic(new Bool()));
            emit(new Xor(immed_1, immed_2));
            topStackAssign(immed_2);
            return null;
        }

        @Override
        public Void visit(EMul p, Void arg) {
            p.expr_1.accept(new ExprVisitor(), arg);
            String immed_1 = currentPointer;

            p.expr_2.accept(new ExprVisitor(), arg);
            String immed_2 = currentPointer;
            Type type = currentPointerType;

            String immed_3 = getPointer(type);
            if(p.mulop_ instanceof Times) {
                emit(new Mul(type, immed_1, immed_2, immed_3));
            } else if(p.mulop_ instanceof Div) {
                emit(new Divide(type, immed_1, immed_2, immed_3));
            } else if(p.mulop_ instanceof Mod) {
                emit(new Modul(type, immed_1, immed_2, immed_3));
            }
            return null;
        }

        @Override
        public Void visit(EAdd p, Void arg) {
            p.expr_1.accept(new ExprVisitor(), arg);
            String immed_1 = currentPointer;

            p.expr_2.accept(new ExprVisitor(), arg);
            String immed_2 = currentPointer;
            Type type = currentPointerType;

            String immed_3 = getPointer(type);
            if(p.addop_ instanceof Plus) {
                emit(new Add(type, immed_1, immed_2, immed_3));
            } else if(p.addop_ instanceof Minus) {
                emit(new Sub(type, immed_1, immed_2, immed_3));
            }
            return null;
        }

        @Override
        public Void visit(ERel p, Void arg) {
            p.expr_1.accept(new ExprVisitor(), arg);
            String immed_1 = currentPointer;

            p.expr_2.accept(new ExprVisitor(), arg);
            String immed_2 = currentPointer;
            Type type = currentPointerType;

            String immed_3 = getPointer(new TBasic(new Bool()));
            if(p.relop_ instanceof LTH) {
                emit(new IfLt(type, immed_1, immed_2, immed_3));
            } else if(p.relop_ instanceof LE) {
                emit(new IfLe(type, immed_1, immed_2, immed_3));
            } else if(p.relop_ instanceof GTH) {
                emit(new IfGt(type, immed_1, immed_2, immed_3));
            } else if(p.relop_ instanceof GE) {
                emit(new IfGe(type, immed_1, immed_2, immed_3));
            } else if(p.relop_ instanceof EQU) {
                emit(new IfEq(type, immed_1, immed_2, immed_3));
            } else if(p.relop_ instanceof NE) {
                emit(new IfNe(type, immed_1, immed_2, immed_3));
            }

            Label lTrue = makeLabel();
            Label lFalse = makeLabel();
            Label lOut = makeLabel();
            emit(new Branch(currentPointer, lTrue, lFalse));
            booleanEnding(lTrue, lFalse, lOut);
            return null;
        }

        @Override
        public Void visit(EAnd p, Void arg) {
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_1.accept(new ExprVisitor(), arg);

            Label mid = makeLabel();
            Label lFalse = makeLabel();
            topStackEval();
            emit(new Branch(currentPointer, mid, lFalse));

            emit(mid);
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_2.accept(new ExprVisitor(), arg);

            Label lTrue = makeLabel();
            Label lOut = makeLabel();
            topStackEval();
            emit(new Branch(currentPointer, lTrue, lFalse));
            booleanEnding(lTrue, lFalse, lOut);
            return null;
        }

        @Override
        public Void visit(EOr p, Void arg) {
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_1.accept(new ExprVisitor(), arg);

            Label lTrue = makeLabel();
            Label mid = makeLabel();
            topStackEval();
            emit(new Branch(currentPointer, lTrue, mid));

            emit(mid);
            newVar(BOOL+stackCounter, new TBasic(new Bool()), true);
            p.expr_2.accept(new ExprVisitor(), arg);

            Label lFalse = makeLabel();
            Label lOut = makeLabel();
            topStackEval();
            emit(new Branch(currentPointer, lTrue, lFalse));
            booleanEnding(lTrue, lFalse, lOut);
            return null;
        }

        @Override
        public Void visit(EAss p, Void arg) {
            String immed_1;
            Type type;
            if(p.expr_1 instanceof EVar) {
                immed_1 = lookupAddress(((EVar) p.expr_1).ident_);
                type = lookupType(immed_1);
            } else {
                p.expr_1.accept(new ExprVisitor(), arg);
                immed_1 = VAR_SIGN + "t" + (address - 2);
                type = currentPointerType;
            }
            p.expr_2.accept(new ExprVisitor(), arg);
            if(!PrimitiveFunctions.isThisArray(type)) {
                String immed_2 = currentPointer;
                emit(new Store(type, immed_2, immed_1));
            } else {
                assignArray(immed_1, p.expr_2);
            }
            return null;
        }
    }





    private class ArgsVisitor implements Arg.Visitor<Void,Void> {
        int index;
        ArgsVisitor(int index) {
            this.index = index;
        }
        public Void visit(Javalette.Absyn.Argument p, Void arg) {
            if(!PrimitiveFunctions.isThisArray(p.type_)) {
                newVar(p.ident_, p.type_, false);
                emit(new Store(p.type_, "%_p_" + p.ident_, lookupAddress(p.ident_)));
            } else {
                newArrayArg(p.ident_, p.type_);
            }
            return null;
        }
    }

    /*declaration has init and no-init*/
    private class ItemVisitor implements Item.Visitor<Void,Void> {

        Type itemType;

        ItemVisitor(Type type) {
            this.itemType = type;
        }

        @Override
        public Void visit(NoInit p, Void arg) {
            newVar(p.ident_, itemType, false);
            return null;
        }

        @Override
        public Void visit(Init p, Void arg) {
            p.expr_.accept(new ExprVisitor(), arg);
            newVar(p.ident_, itemType, false);
            if(!PrimitiveFunctions.isThisArray(itemType)) {
                emit(new Store(itemType, currentPointer, lookupAddress(p.ident_)));
            } else {
                assignArray(lookupAddress(p.ident_), p.expr_);
            }
            return null;
        }
    }



    /**FUNCTIONS AND TOOLS*/
    private void emit(Code c) {
        String code = c.accept(new CodeToJVM());
        if(c instanceof Entry || c instanceof Label) {
            isWithLabel = true;
        } else {
            if (isWithLabel) {
                code = TAB + code;
                isWithLabel = false;
            } else {
                code = DOUBLE_TAB + code;
            }
        }
        output.add(code);
    }

    private void newVar(String id, Type t, boolean isStackVariable) {
        String name = id;
        if(!isStackVariable)
            name = VAR_SIGN + id + "_" + blockLevel;
        registerTable.get(0).put(id, name);
        varsType.get(0).put(name, t);
        if(isStackVariable) {
            booleanStack.add(0, name);
            stackCounter++;
        }
        if(!PrimitiveFunctions.isThisArray(t)) {
            emit(new Alloca(t, name));
            initValue(name, t);
        }
    }

    private void newArrayArg(String id, Type t) {
        String name = VAR_SIGN + "_p_" + id;
        registerTable.get(0).put(id, name);
        varsType.get(0).put(name, t);
    }

    private void initValue(String name, Type type) {
        if(getBasic(type) instanceof Int || getBasic(type) instanceof Bool) {
            emit(new Store(type,"0",name));
        } else if(getBasic(type) instanceof Doub) {
            emit(new Store(type,"0.0",name));
        }
    }

    private void assignArray(String var, Expr expr_) {
        emit(new Calloc(OBJECT, var, nitems, size));
        String value = currentPointer;// array length or pointer
        if(expr_ instanceof ENew) {
            // set new empty array and length
            String immed = getPointer(currentPointerType);
            emit(new ArrLen(OBJECT, immed, var));
            emit(new Store(currentPointerType, value, immed));
        } else if(expr_ instanceof EApp || expr_ instanceof EVar) {
            // pointer to new object array provided
            String immed = getPointer(currentPointerType);
            emit(new LoadObj(OBJECT, immed, value));
            emit(new StoreObj(OBJECT, immed, var));
        }
        anyArray = true;
    }

    private void addArrayObject() {
        if(anyArray) {
            Code code_1 = new Array(OBJECT, SIZE);
            Code code_2 = new Calloc(OBJECT, null, nitems, size);
            Code code_3 = new Free(OBJECT, null);
            String arrayDeclaration = code_1.accept(new CodeToJVM()) +
                    code_2.accept(new CodeToJVM()) + code_3.accept(new CodeToJVM());
            output.add(0, arrayDeclaration);
        }
    }

    private String lookupAddress(String id) {
        if(registerTable.get(0).containsKey(id)) {
            return registerTable.get(0).get(id);
        }
        return null;
    }

    private String getPointer(Type type) {
        String register = VAR_SIGN + "t";
        register = register + address;
        currentPointer = register;
        currentPointerType = type;
        address++;
        return register;
    }

    private Type lookupType(String id) {
        if(varsType.get(0).containsKey(id)) {
            return varsType.get(0).get(id);
        }
        return null;
    }

    private Label makeLabel() {
        Label label = new Label(labelCounter);
        labelCounter++;
        return label;
    }

    // only to assign boolean value into the variable on top of the stack
    private void topStackAssign(String value) {
        if(!booleanStack.isEmpty()) {
            emit(new Store(new TBasic(new Bool()), value, lookupAddress(booleanStack.get(0))));
        }
    }

    private void topStackEval() {
        if(!booleanStack.isEmpty()) {
            emit(new Load(new TBasic(new Bool()),
                    getPointer(new TBasic(new Bool())),
                    lookupAddress(booleanStack.get(0))));
            // remove the top of the stack after use
            booleanStack.remove(0);
        }
    }

    private void topStackRemove() {
        if(!booleanStack.isEmpty()) {
            booleanStack.remove(0);
        }
    }

    private void addBlockLevel() {
        Map<String,Type> vars = varsType.get(0);
        Map<String,String> addr = registerTable.get(0);
        varsType.add(0, new TreeMap<>(vars));
        registerTable.add(0, new TreeMap<>(addr));
        blockLevel++;
    }

    private void removeTopBlock() {
        Map<String,Type> vars = varsType.get(1);
        Map<String,String> addr = registerTable.get(1);
        varsType.set(0, new TreeMap<>(vars));
        varsType.remove(1);
        registerTable.set(0, new TreeMap<>(addr));
        registerTable.remove(1);
    }

    private void booleanEnding(Label lTrue, Label lFalse, Label lOut) {
        emit(lTrue);
        topStackAssign("1");
        emit(new Branch(null, lOut, null));
        emit(lFalse);
        topStackAssign("0");
        emit(new Branch(null, lOut, null));
        emit(lOut);
    }

    private void functionEnding(Type returnType) {
        String end = output.get(output.size()-1);
        if(theReturn == null || !end.contains("ret")) {
            // function must end with return!
            if(getBasic(returnType) instanceof Int || getBasic(returnType) instanceof Bool) {
                emit(new Return(returnType, "0"));
            } else if(getBasic(returnType) instanceof Doub) {
                emit(new Return(returnType, "0.0"));
            } else if(getBasic(returnType) instanceof Javalette.Absyn.Void) {
                emit(new Return(returnType, null));
            }
        } else {
            if (isWithLabel) {
                // just to prevent error for empty label
                output.add(TAB + "unreachable\n");
                isWithLabel = false;
            }
        }
    }

    private BasicType getBasic(Type c) {
        return PrimitiveFunctions.extractBasic(c);
    }
}