import Javalette.Absyn.*;
import Javalette.Absyn.Void;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by bamibestelt on 2017-04-18.
 */
public class PrimitiveFunctions {

    private static final String PRINT_INT = "printInt";
    private static final String PRINT_STRING = "printString";
    private static final String PRINT_DOUBLE = "printDouble";
    private static final String READ_INT = "readInt";
    private static final String READ_DOUBLE = "readDouble";


    static Map<String,Fun> getPrimitives() {

        // Put primitive functions into signature
        Map<String,Fun> primitives = new TreeMap<>();;

        ListType printIntType = new ListType();
        printIntType.add(new TBasic(new Int()));
        primitives.put(PRINT_INT, new Fun(new TBasic(new Void()), printIntType));

        ListType printStringType = new ListType();
        printStringType.add(new TBasic(new Str()));
        primitives.put(PRINT_STRING, new Fun(new TBasic(new Void()), printStringType));

        ListType printDoubleType = new ListType();
        printDoubleType.add(new TBasic(new Doub()));
        primitives.put(PRINT_DOUBLE, new Fun(new TBasic(new Void()), printDoubleType));

        primitives.put(READ_INT, new Fun(new TBasic(new Int()), new ListType()));
        primitives.put(READ_DOUBLE, new Fun(new TBasic(new Doub()), new ListType()));

        return primitives;
    }

    /**code to be added at the beginning of the llvm code*/
    static String runtimeDeclaration() {
        String runtime =
                "declare void @printInt(i32)\n" +
                        "declare void @printDouble(double)\n" +
                        "declare void @printString(i8*)\n" +
                        "declare i32 @readInt()\n" +
                        "declare double @readDouble()\n\n";
        return runtime;
    }

    public static Map<String,Func> addRuntimeToSignature(Map<String,Func> sig) {
        ListArg pi = new ListArg();
        pi.add(new Argument(new TBasic(new Int()), "x"));
        String PRINT_INT = "printInt";
        sig.put(PRINT_INT, new Func(PRINT_INT, new TBasic(new Javalette.Absyn.Void()), pi));
        ListArg pd = new ListArg();
        pd.add(new Argument(new TBasic(new Doub()), "x"));
        String PRINT_DOUBLE = "printDouble";
        sig.put(PRINT_DOUBLE, new Func(PRINT_DOUBLE, new TBasic(new Javalette.Absyn.Void()), pd));
        ListArg ps = new ListArg();
        ps.add(new Argument(new TBasic(new Str()), "s"));
        String PRINT_STRING = "printString";
        sig.put(PRINT_STRING, new Func(PRINT_STRING, new TBasic(new Javalette.Absyn.Void()), ps));
        ListArg rd = new ListArg();
        String READ_INT = "readInt";
        sig.put(READ_INT, new Func(READ_INT, new TBasic(new Int()), rd));
        String READ_DOUBLE = "readDouble";
        sig.put(READ_DOUBLE, new Func(READ_DOUBLE, new TBasic(new Doub()), rd));
        return sig;
    }

    public static BasicType extractBasic(Type bs) {
        BasicType bt = null;
        int flag = bs.accept(new TypeFlagVisitor(), null);
        if (flag == TypeFlagVisitor.BASIC_TYPE) {
            bt = ((TBasic) bs).basictype_;
        } else if (flag == TypeFlagVisitor.ARRAY_TYPE) {
            bt = ((TArr)(((TArray) bs).arraytype_)).basictype_;
        }
        return bt;
    }

    public static class BasicTypeToString implements BasicType.Visitor<String, java.lang.Void> {

        @Override
        public String visit(Int p, java.lang.Void arg) {
            return "i32";
        }

        @Override
        public String visit(Doub p, java.lang.Void arg) {
            return "double";
        }

        @Override
        public String visit(Bool p, java.lang.Void arg) {
            return "i1";
        }

        @Override
        public String visit(Str p, java.lang.Void arg) {
            return "i8*";
        }

        @Override
        public String visit(Javalette.Absyn.Void p, java.lang.Void arg) {
            return "void";
        }
    }

    public static boolean isThisArray(Type type) {
        return type.accept(new TypeFlagVisitor(), null) == TypeFlagVisitor.ARRAY_TYPE;
    }
}
