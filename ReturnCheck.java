import Javalette.Absyn.*;

import java.lang.Void;

/**
 * Created by bamibestelt on 2017-04-19.
 */
public class ReturnCheck {



    private boolean isReturnValid = false;



    public void returnCheck(Prog p) {
        Program program = (Program) p;
        program.accept(new ProgramVisitor(), null);
    }



    /** A Program */
    public class ProgramVisitor implements Program.Visitor<Void,Void> {

        @Override
        public Void visit(Program p, Void arg) {

            // Check definitions
            for (TopDef x: p.listtopdef_) {
                x.accept(new DefVisitor(), arg);

                // re-init for new function
                isReturnValid = false;
            }

            return null;
        }
    }


    /** Functions */
    public class DefVisitor implements TopDef.Visitor<Void,Void> {

        @Override
        public Void visit(FnDef p, Void arg) {

            Block block = (Block) p.blk_;
            // check function statements
            for (Stmt s : block.liststmt_) {
                Object ret = s.accept(new StmtVisitor(), arg);
                if(ret != null) isReturnValid = true;
            }

            // TODO get basic type
            BasicType bt = PrimitiveFunctions.extractBasic(p.type_);
            if(!(bt instanceof Javalette.Absyn.Void)) {
                if (!isReturnValid) {
                    throw new ReturnException("missing return statement");
                }
            }

            return null;
        }
    }



    /** Statements */
    public class StmtVisitor implements Stmt.Visitor<Object,Void> {
        @Override
        public Object visit(Empty p, Void arg) {
            return null;
        }

        @Override
        public Object visit(BStmt p, Void arg) {
            Object ret = null;
            Block block = (Block) p.blk_;
            for (Stmt s : block.liststmt_) {
                Object obj = s.accept(new StmtVisitor(), arg);
                if(obj != null) {
                    isReturnValid = true;
                    ret = obj;
                }
            }
            return ret;
        }

        @Override
        public Object visit(Decl p, Void arg) {
            return null;
        }

        @Override
        public Object visit(Incr p, Void arg) {
            return null;
        }

        @Override
        public Object visit(Decr p, Void arg) {
            return null;
        }

        @Override
        public Object visit(Ret p, Void arg) {
            return p;
        }

        @Override
        public Object visit(VRet p, Void arg) {
            return null;
        }

        @Override
        public Object visit(Cond p, Void arg) {
            Object ret = p.stmt_.accept(new StmtVisitor(), arg);
            Object exp = p.expr_.accept(new ExprVisitor(), arg);
            if(exp instanceof ELitTrue && ret != null) {
                isReturnValid = true;
                return ret;
            } else {
                isReturnValid = false;
                return null;
            }
        }

        @Override
        public Object visit(CondElse p, Void arg) {
            Object ret1 = p.stmt_1.accept(new StmtVisitor(), arg);
            Object ret2 = p.stmt_2.accept(new StmtVisitor(), arg);
            Object exp = p.expr_.accept(new ExprVisitor(), arg);
            // we check if it's true or false literals
            // then we only need return in the statement that
            // reachable by the conditional branch
            if(exp instanceof ELitTrue || exp instanceof ELitFalse) {
                if(exp instanceof ELitTrue && ret1 != null) {
                    isReturnValid = true;
                    return ret1;
                } else if(exp instanceof ELitFalse && ret2 != null) {
                    isReturnValid = true;
                    return ret1;
                } else {
                    isReturnValid = false;
                    return null;
                }
            } else {
                // if it's not true or false literals then
                // we have to check if return exists for both statements
                // so to make sure that one of the return terminates
                if(ret1 != null && ret2 != null) {
                    isReturnValid = true;
                    return ret1;
                } else {
                    isReturnValid = false;
                    return null;
                }
            }
        }

        @Override
        public Object visit(While p, Void arg) {
            Object ret = p.stmt_.accept(new StmtVisitor(), arg);
            Object exp = p.expr_.accept(new ExprVisitor(), arg);
            if(exp instanceof ELitTrue && ret != null) {
                isReturnValid = true;
                return ret;
            } else {
                isReturnValid = false;
                return null;
            }
        }

        @Override
        public Object visit(ForEach p, Void arg) {
            // TODO return statement inside for each
            Object ret = p.stmt_.accept(new StmtVisitor(), arg);
            if(ret != null) {
                isReturnValid = true;
                return ret;
            } else {
                isReturnValid = false;
                return null;
            }
        }

        @Override
        public Object visit(SExp p, Void arg) {
            return null;
        }
    }



    /** Expressions */
    public class ExprVisitor implements Expr.Visitor<Object,Void> {
        @Override
        public Object visit(EVar p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(ELitInt p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(ELitDoub p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(ELitTrue p, Void arg) {
            return p;
        }

        @Override
        public Object visit(ELitFalse p, Void arg) {
            return p;
        }

        @Override
        public Object visit(EApp p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EString p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EArrLit p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EArrLen p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(ENew p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(Neg p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(Not p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EMul p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EAdd p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(ERel p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EAnd p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EOr p, Void arg) {
            return new Object();
        }

        @Override
        public Object visit(EAss p, Void arg) {
            return new Object();
        }
    }
}