package Javalette.Absyn; // Java Package generated by the BNF Converter.

public class TArr extends ArrayType {
  public final BasicType basictype_;
  public final ListDimensions listdimensions_;
  public TArr(BasicType p1, ListDimensions p2) { basictype_ = p1; listdimensions_ = p2; }

  public <R,A> R accept(Javalette.Absyn.ArrayType.Visitor<R,A> v, A arg) { return v.visit(this, arg); }

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o instanceof Javalette.Absyn.TArr) {
      Javalette.Absyn.TArr x = (Javalette.Absyn.TArr)o;
      return this.basictype_.equals(x.basictype_) && this.listdimensions_.equals(x.listdimensions_);
    }
    return false;
  }

  public int hashCode() {
    return 37*(this.basictype_.hashCode())+this.listdimensions_.hashCode();
  }


}
