import Javalette.Absyn.*;

import java.lang.Void;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TypeChecker {

    // Global signature of functions
    private Map<String,Fun> sig;

    // Stack of contexts
    private List<Map<String,Type>> cxt;

    // Return type of function we are checking
    private Type returnType;

    // Share type constants
    private final BasicType BOOL   = new Bool();
    private final BasicType INT    = new Int();
    private final BasicType DOUBLE = new Doub();
    private final BasicType VOID   = new Javalette.Absyn.Void();
    private final BasicType STRING   = new Str();
    
    // Entry point
    public void typeCheck(Prog p) {
        sig = PrimitiveFunctions.getPrimitives();

        Program program = (Program) p;
        program.accept(new ProgramVisitor(), null);
    }





    ////////////////////////////// Program //////////////////////////////

    public class ProgramVisitor implements Program.Visitor<Void,Void> {
        public Void visit(Javalette.Absyn.Program p, Void arg) {

            // Extend signature by all the definitions
            for (TopDef x: p.listtopdef_) {
                FnDef d = (FnDef)x;
                // make sure x is not already in the signature.
                if (sig.get(d.ident_) != null)
                    throw new TypeException("function " + d.ident_ + " has already been declared");

                ListType argsType = new ListType();
                for (Arg y : d.listarg_) {
                    Type aType = y.accept(new ArgdVisitor(), arg);
                    argsType.add(aType);
                }

                sig.put(d.ident_, new Fun(d.type_, argsType));
            }

            // Check definitions
            for (TopDef x: p.listtopdef_) {
                returnType = sig.get(((FnDef)x).ident_).type_;
                x.accept(new DefVisitor(), arg);
            }

            Fun ft = sig.get("main");
            if (ft == null)
                throw new TypeException("main function is missing");
            else if(!PrimitiveFunctions.extractBasic(ft.type_).equals(INT))
                throw new TypeException("main have to be int");
            else if (!ft.listtype_.isEmpty())
                throw new TypeException("main function can not have argument");
            return null;
        }
    }


    ////////////////////////////// Function //////////////////////////////

    public class DefVisitor implements TopDef.Visitor<Void,Void> {
        public Void visit(Javalette.Absyn.FnDef p, Void arg) {
            // set return type and initial context
            returnType = p.type_;
            cxt = new LinkedList();
            cxt.add(new TreeMap());

            // add all function parameters to context
            for (Arg a: p.listarg_) {
                a.accept(new ArgVisitor(), arg);
            }

            if(p.blk_ != null) {
                Block block = (Block) p.blk_;
                // check function statements
                for (Stmt s : block.liststmt_) {
                    s.accept(new StmVisitor(), arg);
                }
            } else {
                throw new TypeException("main function has missing body");
            }
            return null;
        }
    }

    ///////////////////////// Function argument /////////////////////////

    // Add a type declaration to the context
    private class ArgVisitor implements Arg.Visitor<Type,Void> {
        public Type visit(Javalette.Absyn.Argument p, Void arg) {
            newVar(p.ident_, p.type_);
            return p.type_;
        }
    }

    private class ArgdVisitor implements Arg.Visitor<Type,Void> {
        public Type visit(Javalette.Absyn.Argument p, Void arg) {
            return p.type_;
        }
    }





    ////////////////////////////// Statement //////////////////////////////

    private class StmVisitor implements Stmt.Visitor<Void,Void> {
        public Void visit(Javalette.Absyn.Empty p, Void arg) {
            return null;
        }

        public Void visit(Javalette.Absyn.SExp p, Void arg) {
            Type t = p.expr_.accept(new ExpVisitor(), arg);
            return null;
        }

        public Void visit(Javalette.Absyn.Decl p, Void arg) {
            for(Item item: p.listitem_) {
                item.accept(new ItemVisitor(p.type_), arg);
            }
            return null;
        }

        // post-increment, post-decrement
        // x++
        @Override
        public Void visit(Incr p, Void arg) {
            if(p.ident_ == null)
                throw new TypeException("Increment operation needs variable");

            return null;
        }

        // x--
        @Override
        public Void visit(Decr p, Void arg) {
            if(p.ident_ == null)
                throw new TypeException("Decrement operation needs variable");

            return null;
        }

        // return with expression
        public Void visit(Javalette.Absyn.Ret p, Void arg) {
            Type t1 = p.expr_.accept(new ExpVisitor(),arg);
            equalArray(t1, returnType);
            equalTypes(t1, returnType);
            // TODO
            /*if(!t1.equals(returnType))
                throw new TypeException("Function must have return value with the same type");*/
            return null;
        }

        // just return, no expression
        public Void visit(Javalette.Absyn.VRet p, Void arg) {
            check(new TBasic(VOID), returnType);
            // TODO
            /*if(!returnType.equals(VOID))
                throw new TypeException("Function must have return value with the same type");*/
            return null;
        }

        public Void visit(Javalette.Absyn.While p, Void arg) {
            Type t1= p.expr_.accept(new ExpVisitor(),arg);
            check(new TBasic(BOOL), t1);

            if(p.stmt_ == null) {
                throw new TypeException("While is missing statement");
            } else {
                // secure the scope of if and else
                newBlock();
                p.stmt_.accept(new StmVisitor(), arg);
                popBlock();
            }

            return null;
        }

        @Override
        public Void visit(ForEach p, Void arg) {
            Type aType = lookupVar(p.ident_2);
            checkArray(aType);// must be an array variable
            check(p.type_, aType);// must have same basic type
            return null;
        }

        // E.g. int x; { int x = 1; x++; }
        public Void visit(Javalette.Absyn.BStmt p, Void arg) {
            Block block = (Block) p.blk_;

            newBlock();
            for (Stmt s: block.liststmt_) s.accept(new StmVisitor(), arg);
            popBlock();
            return null;
        }

        // if condition without else
        public Void visit(Javalette.Absyn.Cond p, Void arg) {
            Type t1= p.expr_.accept(new ExpVisitor(),arg);
            check(new TBasic(BOOL), t1);

            if(p.stmt_ == null) {
                throw new TypeException("If is missing statement");
            } else {
                // secure the scope of if and else
                newBlock();
                p.stmt_.accept(new StmVisitor(), arg);
                popBlock();
            }

            return null;
        }

        // if condition with else
        public Void visit(Javalette.Absyn.CondElse p, Void arg) {
            Type t1= p.expr_.accept(new ExpVisitor(),arg);
            check(new TBasic(BOOL), t1);

            if(p.stmt_1 == null) {
                throw new TypeException("If condition is missing statement");
            } else {
                // secure the scope of if and else
                newBlock();
                p.stmt_1.accept(new StmVisitor(), arg);
                popBlock();
            }

            if(p.stmt_2 == null) {
                throw new TypeException("Else condition is missing statement");
            } else {
                newBlock();
                p.stmt_2.accept(new StmVisitor(), arg);
                popBlock();
            }
            return null;
        }
    }



    /*declaration has init and no-init*/
    private class ItemVisitor implements Item.Visitor<Void,Void> {

        Type itemType;

        public ItemVisitor(Type type) {
            this.itemType = type;
        }

        @Override
        public Void visit(NoInit p, Void arg) {
            newVar(p.ident_, itemType);
            return null;
        }

        @Override
        public Void visit(Init p, Void arg) {
            //E.g. "int x = 1";
            Type tExpr = p.expr_.accept(new ExpVisitor(), arg);
            equalArray(itemType, tExpr);
            equalTypes(itemType, tExpr);
            newVar(p.ident_, itemType);
            return null;
        }
    }





    ////////////////////////////// Expression //////////////////////////////

    private class ExpVisitor implements Expr.Visitor<Type,Void> {

        // Literals
        public Type visit(Javalette.Absyn.ELitTrue p, Void arg) {
            return new TBasic(BOOL);
        }
        public Type visit(Javalette.Absyn.ELitFalse p, Void arg) {
            return new TBasic(BOOL);
        }
        public Type visit(Javalette.Absyn.ELitInt p, Void arg) {
            return new TBasic(INT);
        }
        public Type visit(Javalette.Absyn.ELitDoub p, Void arg) {
            return new TBasic(DOUBLE);
        }

        // Variable
        public Type visit(Javalette.Absyn.EVar p, Void arg) {
            return lookupVar(p.ident_);
        }

        // Function call  plus(4,3)  where  int plus(int x, int y);
        public Type visit(Javalette.Absyn.EApp p, Void arg) {
            Fun ft = sig.get(p.ident_);
            if (ft == null)
                throw new TypeException("Undefined function " + p.ident_);
            if (ft.listtype_.size() != p.listexpr_.size())
                throw new TypeException("Function " + p.ident_ + " not applied to correct number of arguments");

            // Check function arguments
            int i = 0;
            for (Expr e: p.listexpr_) {
                Type a = ft.listtype_.get(i);
                check(a, e.accept(new ExpVisitor(), arg));
                i++;
            }
            return ft.type_;
        }

        @Override
        public Type visit(EString p, Void arg) {
            return new TBasic(STRING);
        }

        @Override
        public Type visit(EArrLit p, Void arg) {
            Type arr = lookupVar(p.ident_);
            checkArray(arr);// array only

            Type t1 = p.expr_.accept(new ExpVisitor(), arg);
            check(new TBasic(INT), t1);// array index must be integer
            BasicType bt = ((TArr)(((TArray) arr).arraytype_)).basictype_;
            // return the type of the value not the type of the array
            return new TBasic(bt);
        }

        @Override
        public Type visit(EArrLen p, Void arg) {
            Type t = lookupVar(p.ident_);
            checkArray(t);
            return new TBasic(INT);
        }

        @Override
        public Type visit(ENew p, Void arg) {
            for(DimExpr dimExpr : p.listdimexpr_) {
                // how to get the value size from expression?
                DExpr dExpr = (DExpr) dimExpr;
                Type type = dExpr.expr_.accept(new ExpVisitor(), arg);
                check(new TBasic(INT), type);
            }
            BasicType bt = PrimitiveFunctions.extractBasic(p.type_);
            return new TArray(new TArr(bt, null));// we should know it is an array
        }

        @Override
        public Type visit(Neg p, Void arg) {
            Type t1 = p.expr_.accept(new ExpVisitor(), arg);
            return numericType(t1);
        }

        @Override
        public Type visit(Not p, Void arg) {
            Type t1 = p.expr_.accept(new ExpVisitor(), arg);
            check(new TBasic(BOOL), t1);
            return new TBasic(BOOL);
        }

        // Arithmetical operators
        public Type visit(Javalette.Absyn.EMul p, Void arg) {
            // multiplication, division, and modulus are one category
            Type t1=p.expr_1.accept(new ExpVisitor(), arg);
            Type t2=p.expr_2.accept(new ExpVisitor(), arg);

            equalTypes(t1, t2);
            Type t = numericType(t1);
            BasicType bt1 = PrimitiveFunctions.extractBasic(t1);
            if(p.mulop_ instanceof Mod && !bt1.equals(INT)) {
                // type for modulus is overloaded
                throw new TypeException("Modulus requires operands with the same integer type");
            }
            return t;
        }

        public Type visit(Javalette.Absyn.EAdd p, Void arg) {
            // addition, and subtraction are one category
            Type t1=p.expr_1.accept(new ExpVisitor(), arg);
            Type t2=p.expr_2.accept(new ExpVisitor(), arg);

            equalTypes(t1, t2);
            return numericType(t1);
        }

        // Comparison operators
        public Type visit(Javalette.Absyn.ERel p, Void arg) {
            Type t1=p.expr_1.accept(new ExpVisitor(), arg);
            Type t2=p.expr_2.accept(new ExpVisitor(), arg);

            RelOp operator = p.relop_;
            if(operator instanceof EQU || operator instanceof NE) {
                equalTypes(t1, t2);
                allType(t1);
                return new TBasic(BOOL);
            } else {
                equalTypes(t1, t2);
                numericType(t1);
                return new TBasic(BOOL);
            }
        }

        // Lazy evaluation operators
        public Type visit(Javalette.Absyn.EAnd p, Void arg) {
            Type t1 = p.expr_1.accept(new ExpVisitor(), arg);
            Type t2 = p.expr_2.accept(new ExpVisitor(), arg);
            BasicType bt1 = PrimitiveFunctions.extractBasic(t1);
            BasicType bt2 = PrimitiveFunctions.extractBasic(t2);
            if (!bt1.equals(BOOL) && !bt2.equals(BOOL))
                throw new TypeException("&& requires both operands to be Boolean!");
            else
                return new TBasic(BOOL);
        }

        public Type visit(Javalette.Absyn.EOr p, Void arg) {
            Type t1=p.expr_1.accept(new ExpVisitor(), arg);
            Type t2=p.expr_2.accept(new ExpVisitor(), arg);
            BasicType bt1 = PrimitiveFunctions.extractBasic(t1);
            BasicType bt2 = PrimitiveFunctions.extractBasic(t2);
            if(!bt1.equals(BOOL)&& !bt2.equals(BOOL))
                throw new TypeException("|| requires both operands to be Boolean!");
            else
                return new TBasic(BOOL);
        }

        @Override
        public Type visit(EAss p, Void arg) {
            Type t1 = p.expr_1.accept(new ExpVisitor(), arg);
            Type t2 = p.expr_2.accept(new ExpVisitor(), arg);
            if(t1 == null) {
                throw new TypeException("Assignment error. Unidentified type of assignment target!");
            }else{
                equalArray(t1, t2);
                equalTypes(t1, t2);
            }
            return null;
        }
    }





    ///////////////////////// Context handling /////////////////////////

    public void newVar(String x, Type t) {
        // Get the top context block
        Map<String,Type> m = cxt.get(0);

        // Check that the variable is not declared already
        if (m.containsKey(x))
            throw new TypeException("duplicate variable binding " + x);

        // Add the binding
        m.put(x, t);
    }

    public void newBlock() {
        cxt.add(0, new TreeMap());
    }
    public void popBlock() {
        cxt.remove(0);
    }

    public Type lookupVar(String x) {
        Type t = null;
        for (Map<String,Type> m : cxt) {
            t = m.get(x);
            if (t != null) {
                return t;
            }
        }

        if(t == null) {
            throw new TypeException("unbound variable " + x);
        }
        return t;
    }





    ////////////////////////////// Exp / Type shape //////////////////////////////

    // Expected type: t
    // Inferred type: u
    public void check (BasicType t, BasicType u) {
        if (!t.equals(u))
            throw new TypeException("expected type " + t + ", but found type " + u);
    }

    // check basic type, no need to know it is array or not
    public void check(Type t, Type u) {
        BasicType bt_1 = PrimitiveFunctions.extractBasic(t);
        BasicType bt_2 = PrimitiveFunctions.extractBasic(u);
        check(bt_1, bt_2);
    }

    // check if this type is an array type or not
    public void checkArray (Type a) {
        int flag = a.accept(new TypeFlagVisitor(), null);
        if (flag != TypeFlagVisitor.ARRAY_TYPE)
            throw new TypeException("array expected");
    }

    // check if both types are array
    // Creating an array may or may not be combined with the declaration
    public void equalArray (Type a, Type b) {
        int flag1 = a.accept(new TypeFlagVisitor(), null);
        int flag2 = b.accept(new TypeFlagVisitor(), null);
        if(flag1 == TypeFlagVisitor.ARRAY_TYPE || flag2 == TypeFlagVisitor.ARRAY_TYPE) {
            if (flag1 != flag2)
                throw new TypeException("array expected");
        } else {
            // do nothing
        }
    }

    public Type allType (Type t) {
        BasicType bt = PrimitiveFunctions.extractBasic(t);
        if (!bt.equals(INT) && !bt.equals(DOUBLE) && !bt.equals(BOOL))
            throw new TypeException("expected int, double or boolean type");
        return new TBasic(bt);
    }

    public Type numericType (Type t) {
        BasicType bt = PrimitiveFunctions.extractBasic(t);
        if (!bt.equals(INT) && !bt.equals(DOUBLE))
            throw new TypeException("expected expression of numeric type");
        return new TBasic(bt);
    }

    public void equalTypes(Type t1, Type t2) {
        BasicType bt_1 = PrimitiveFunctions.extractBasic(t1);
        BasicType bt_2 = PrimitiveFunctions.extractBasic(t2);
        if (!bt_1.equals(bt_2))
            throw new TypeException("expected types " + t1 + " and " + t2 + " to be equal");
    }

}