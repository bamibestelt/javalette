import Javalette.Yylex;
import Javalette.parser;

import java.io.FileReader;
import java.io.IOException;

public class jlc {
    public static void main(String args[]) {
        if (args.length != 1) {
            System.err.println("Usage: jlc <SourceFile>");
            System.exit(1);
        }

        Yylex l = null;
        try {
            String path = args[0];
            l = new Yylex(new FileReader(path));
            parser p = new parser(l);
            Javalette.Absyn.Prog parse_tree = p.pProg();
            new TypeChecker().typeCheck(parse_tree);
            new ReturnCheck().returnCheck(parse_tree);
            new CodeGenerator().generate(path, parse_tree);
            System.err.println("OK");
            System.exit(0);
        } catch (TypeException e) {
            // TYPE ERROR from type checker
            System.err.println("ERROR");
            System.err.println(e.toString());
            System.exit(1);
        } catch (ReturnException e) {
            // RETURN ERROR from return checker
            System.err.println("RETURN ERROR");
            System.err.println(e.toString());
            System.exit(1);
        } catch (IOException e) {
            System.err.println("ERROR");
            System.err.println(e.toString());
            System.exit(1);
        } catch (Throwable e) {
            // SYNTAX ERROR from parser
            System.err.println("ERROR");
            System.err.println(e.toString());
            System.exit(1);
        }
    }
}
