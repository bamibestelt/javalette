public class ReturnException extends RuntimeException {

    public ReturnException(String msg) {
        super(msg);
    }

}
