import Javalette.Absyn.*;

import java.lang.Void;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by bamibestelt on 2017-04-24.
 */
public class Func {

    private Type functionType;
    private String functionName;
    private LinkedList<Arg> functionArgs;

    public Func(String id, Type type, ListArg listArg) {
        this.functionName = id;
        this.functionType = type;
        this.functionArgs = listArg;
    }

    public LinkedList<Arg> getFunctionArgs() {
        return functionArgs;
    }

    public Type getFunctionType() {
        return functionType;
    }

    // function in llvm: define i32 @fact(i32 %n, i32 %m) {}
    // function in llvm: define i32 @fact(i32 %_p_n, i32 %_p_m) {}
    public String toLLVM() {
        String returnType = functionType.accept(new TypeToString(), null);
        String llvm_func = "define " + returnType + " @" + functionName;
        llvm_func = llvm_func + genArguments(new LinkedList<>()) + " {";
        return llvm_func;
    }

    public String callToLLVM(LinkedList<String> values) {
        String llvm_call = "call " + functionType.accept(new TypeToString(), null) + " @" + functionName;
        llvm_call = llvm_call + genArguments(values) + "\n";
        return llvm_call;
    }

    private String genArguments(LinkedList<String> values) {
        int index = 0;
        String param;
        String paramList = "";
        for (Arg a: functionArgs) {
            Argument arg = (Argument) a;
            if(values.isEmpty()) {
                param = arg.type_.accept(new TypeToString(), null)
                        + " " + "%_p_" + arg.ident_ + ", ";
            } else {
                param = arg.type_.accept(new TypeToString(), null)
                        + " " + values.get(index) + ", ";
            }
            paramList = paramList + param;
            index++;
        }
        paramList = paramListFormatting(paramList);
        return paramList;
    }

    private String paramListFormatting(String paramList) {
        if(!paramList.isEmpty())
            paramList = paramList.substring(0, paramList.length() - 2);// remove the last ,_

        paramList = "(" + paramList + ")";
        return paramList;
    }





    public class TypeToString implements Type.Visitor<String,Void> {

        @Override
        public String visit(TBasic p, Void arg) {
            return p.basictype_.accept
                    (new PrimitiveFunctions.BasicTypeToString(), arg);
        }

        @Override
        public String visit(TArray p, Void arg) {
            String type = "%object*";
            return type;
        }

        @Override
        public String visit(Fun p, Void arg) {
            return null;
        }
    }
}
