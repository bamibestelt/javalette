import Javalette.Absyn.Fun;
import Javalette.Absyn.TArray;
import Javalette.Absyn.TBasic;
import Javalette.Absyn.Type;

/**
 * Created by bamibestelt on 2017-05-20.
 */
public class TypeFlagVisitor implements Type.Visitor<Integer,Void> {

    public static final int BASIC_TYPE = 7;
    public static final int ARRAY_TYPE = 8;
    public static final int FUN_TYPE = 9;

    @Override
    public Integer visit(TBasic p, Void arg) {
        return BASIC_TYPE;
    }

    @Override
    public Integer visit(TArray p, Void arg) {
        return ARRAY_TYPE;
    }

    @Override
    public Integer visit(Fun p, Void arg) {
        return FUN_TYPE;
    }
}
