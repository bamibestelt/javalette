import Javalette.Absyn.*;

import java.util.LinkedList;

/**
 * Created by bamibestelt on 2017-04-24.
 */
abstract class Code {
    public abstract <R> R accept (CodeVisitor<R> v);
}

class Alloca extends Code {
    public Type type;
    public String var;
    public Alloca (Type type, String var) {
        this.type = type;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Store extends Code {
    public Type type;
    public String immed;
    public String var;
    public Store (Type type, String immed, String var) {
        this.type = type;
        this.immed = immed;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Load extends Code {
    public Type type;
    public String immed;
    public String var;
    public Load (Type type, String immed, String var) {
        this.type = type;
        this.immed = immed;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}


class Return extends Code {
    public Type type;
    public String immed;
    public Return (Type type, String immed) {
        this.type = type;
        this.immed = immed;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Call extends Code {
    public Func func;
    public String immed;
    public LinkedList<String> listImmed;

    /**@param listImmed is the list of value for params,
     * can be immediate register variable or a literal value
     * */
    public Call(Func func, String immed, LinkedList listImmed) {
        this.func = func;
        this.immed = immed;
        this.listImmed = listImmed;
    }
    public <R> R accept(CodeVisitor<R> v) {
        return v.visit(this);
    }
}

class Entry extends Code {
    public Entry() {}
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Label extends Code {
    public Integer labelNumber;
    public Label (Integer label) {
        this.labelNumber = label;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Branch extends Code {
    public Label label_1;
    public Label label_2;
    public String immed;
    public Branch (String immed, Label label_1, Label label_2) {
        this.immed = immed;
        this.label_1 = label_1;
        this.label_2 = label_2;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfEq extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfEq(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfNe extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfNe(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfLt extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfLt(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfGt extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfGt(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfLe extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfLe(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class IfGe extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public IfGe(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Inc extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String var;
    /**translating increment operation into llvm sequence
     * that includes add and store.
     * @param immed_2 is the target address
     * @param immed_1 is the variable address to increment
     * larger addres is always the target address in llvm assignment*/
    public Inc(Type type, String immed_1, String immed_2, String var) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Dec extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String var;
    public Dec(Type type, String immed_1, String immed_2, String var) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Add extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public Add(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Sub extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public Sub(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Mul extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public Mul(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Divide extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public Divide(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Modul extends Code {
    public Type type;
    public String immed_1;
    public String immed_2;
    public String immed_3;
    public Modul(Type type, String immed_1, String immed_2, String immed_3) {
        this.type = type;
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
        this.immed_3 = immed_3;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Xor extends Code {
    public String immed_1;
    public String immed_2;
    public Xor(String immed_1, String immed_2) {
        this.immed_1 = immed_1;
        this.immed_2 = immed_2;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class StringArr extends Code {
    public int length;
    public String label;
    public String string;
    public String immed;
    public StringArr (String label, int length, String string, String immed) {
        this.label = label;
        this.length = length + 1;// TODO because we add \00
        this.string = string;
        this.immed = immed;// target of this string
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Array extends Code {
    public String object;
    public int size;
    public Array (String object, int size) {
        this.object = object;
        this.size = size;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Calloc extends Code {
    public String object;
    public String var;
    public int nitems;
    public int size;
    public Calloc (String object, String var, int nitems, int size) {
        this.object = object;
        this.var = var;// TODO if variable exist then it's a call
        this.nitems = nitems;
        this.size = size;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class Free extends Code {
    public String object;
    public String var;
    public Free (String object, String var) {
        this.object = object;
        this.var = var;// TODO if variable exist then it's a call
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class ArrElm extends Code {
    public String object;
    public String immed;
    public String var;
    public String index;
    public Type type;
    public ArrElm (String object, String immed, String var, String index, Type type) {
        this.object = object;
        this.immed = immed;
        this.var = var;
        this.index = index;
        this.type = type;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class ArrLen extends Code {
    public String object;
    public String immed;
    public String var;
    public ArrLen (String object, String immed, String var) {
        this.object = object;
        this.immed = immed;
        this.var = var;// TODO ENew will use this to set array length
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class StoreObj extends Code {
    public String object;
    public String immed;
    public String var;
    public StoreObj (String object, String immed, String var) {
        this.object = object;
        this.immed = immed;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class LoadObj extends Code {
    public String object;
    public String immed;
    public String var;
    public LoadObj (String object, String immed, String var) {
        this.object = object;
        this.immed = immed;
        this.var = var;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}

class ReturnObj extends Code {
    public String object;
    public String immed;
    public ReturnObj (String object, String immed) {
        this.object = object;
        this.immed = immed;
    }
    public <R> R accept (CodeVisitor<R> v) {
        return v.visit (this);
    }
}


interface CodeVisitor<R> {
    public R visit(Alloca c);
    public R visit(Store c);
    public R visit(Load c);
    public R visit(Return c);
    public R visit(Call c);
    public R visit(Entry c);
    public R visit(Label c);
    public R visit(Branch c);
    public R visit(IfEq c);
    public R visit(IfNe c);
    public R visit(IfLt c);
    public R visit(IfGt c);
    public R visit(IfLe c);
    public R visit(IfGe c);
    public R visit(Inc c);
    public R visit(Dec c);
    public R visit(Add c);
    public R visit(Sub c);
    public R visit(Mul c);
    public R visit(Divide c);
    public R visit(Modul c);
    public R visit(Xor c);
    public R visit(StringArr c);
    public R visit(Array c);
    public R visit(Calloc c);
    public R visit(Free c);
    public R visit(ArrElm c);
    public R visit(ArrLen c);
    public R visit(StoreObj c);
    public R visit(LoadObj c);
    public R visit(ReturnObj c);
}

class CodeToJVM implements CodeVisitor<String> {


    private final String NEW_LINE = "\n";

    public String visit(Alloca c) {
        String sReturn = c.var + " = alloca ";
        if (getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "i32";
        } else if (getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "double";
        } else if (getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "i1";
        }
        return sReturn + NEW_LINE;
    }

    public String visit (Store c) {
        String sReturn = "store ";
        if (getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "i32 " + c.immed + " , i32* ";
        } else if (getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "double " + c.immed + " , double* ";
        } else if (getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "i1 " + c.immed + " , i1* ";
        }
        sReturn = sReturn + c.var;
        return sReturn + NEW_LINE;
    }

    public String visit (Load c) {
        String sReturn = c.immed + " = load ";
        if (getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "i32 , i32* ";
        } else if (getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "double , double* ";
        } else if (getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "i1 , i1* ";
        }
        sReturn = sReturn + c.var;
        return sReturn + NEW_LINE;
    }

    public String visit (Return c) {
        String sReturn = "ret ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "double ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "i1 ";
        }

        if(getBasic(c.type) instanceof Javalette.Absyn.Void) {
            sReturn = sReturn + "void ";
        } else {
            sReturn = sReturn + c.immed;
        }
        return sReturn + NEW_LINE;
    }

    public String visit (Call c) {
        String sReturn = "";
        if(c.func != null) {
            sReturn = c.func.callToLLVM(c.listImmed);
            if(c.immed != null) {
                sReturn = c.immed + " = " + sReturn;
            }
        }
        return sReturn;
    }

    public String visit(Entry c) {
        return "entry:";
    }

    public String visit (Label c) {
        return "lab" + c.labelNumber + ":";
    }

    public String visit (Branch c) {
        String sReturn = "br ";
        if(c.immed != null) {
            sReturn = sReturn + "i1 " + c.immed;
            sReturn = sReturn + " , label %lab" + c.label_1.labelNumber + " , label %lab" + c.label_2.labelNumber;
        } else {
            sReturn = sReturn + "label %lab" + c.label_1.labelNumber;
        }
        return sReturn + NEW_LINE + NEW_LINE;
    }

    public String visit (IfEq c) {
        String sReturn = c.immed_3 + " = ";
        // comparison type only added at first operand
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp eq i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp eq i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp oeq double ";
        }
        // because the assignment target always has larger register address
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (IfNe c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp ne i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp ne i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp one double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (IfLt c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp slt i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp slt i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp olt double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (IfGt c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp sgt i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp sgt i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp ogt double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (IfLe c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp sle i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp sle i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp ole double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (IfGe c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "icmp sge i32 ";
        } else if(getBasic(c.type) instanceof Bool) {
            sReturn = sReturn + "icmp sge i1 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fcmp oge double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Inc c) {
        // %t7 = add i32 %t6 , 1
        // store i32 %t7 , i32* %i
        String sequence = "";
        String increase = c.immed_2 + " = ";
        String store = "store ";
        if(getBasic(c.type) instanceof Int) {
            increase = increase + "add i32 ";
            store = store + "i32 " + c.immed_2 + " , i32* " + c.var;
        } else if(getBasic(c.type) instanceof Doub) {
            increase = increase + "fadd double ";
            store = store + "double " + c.immed_2 + " , double* " + c.var;
        }
        increase = increase + c.immed_1 + " , 1" + NEW_LINE;
        store = store + NEW_LINE;
        sequence = increase.concat(store);
        return sequence;
    }

    public String visit (Dec c) {
        String sequence = "";
        String increase = c.immed_2 + " = ";
        String store = "store ";
        if(getBasic(c.type) instanceof Int) {
            increase = increase + "sub i32 ";
            store = store + "i32 " + c.immed_2 + " , i32* " + c.var;
        } else if(getBasic(c.type) instanceof Doub) {
            increase = increase + "fsub double ";
            store = store + "double " + c.immed_2 + " , double* " + c.var;
        }
        increase = increase + c.immed_1 + " , 1" + NEW_LINE;
        store = store + NEW_LINE;
        sequence = increase.concat(store);
        return sequence;
    }

    public String visit (Add c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "add i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fadd double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Sub c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "sub i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fsub double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Mul c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {
            sReturn = sReturn + "mul i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fmul double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Divide c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {// TODO udiv, sdiv, fdiv
            sReturn = sReturn + "sdiv i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "fdiv double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Modul c) {
        String sReturn = c.immed_3 + " = ";
        if(getBasic(c.type) instanceof Int) {// TODO modulus urem, srem, frem
            sReturn = sReturn + "srem i32 ";
        } else if(getBasic(c.type) instanceof Doub) {
            sReturn = sReturn + "frem double ";
        }
        sReturn = sReturn + c.immed_1 + ", " + c.immed_2 + NEW_LINE;
        return sReturn;
    }

    public String visit (Xor c) {
        String sReturn = c.immed_2 + " = ";
        sReturn = sReturn + "xor i1 ";

        // %lnot = xor i1 %tobool, true
        sReturn = sReturn + c.immed_1 + ", true" + NEW_LINE;
        return sReturn;
    }

    // ends with \0A\00
    public String visit(StringArr c) {
        String sReturn = "";
        String info = "[" + c.length + " x i8]";
        if(c.immed != null) {
            sReturn = c.immed + " = ";
            sReturn = sReturn + "getelementptr " + info + " , " + info + "* " + c.label + " , i32 0 , i32 0";
        } else {
            sReturn = c.label + " = ";
            sReturn = sReturn + "global " + info + " c\"" + c.string + "\\00\"";
        }
        return sReturn + NEW_LINE;
    }

    // %object = type {i32, [40 x i32], [40 x i1], [40 x double]}
    public String visit(Array c) {
        String sReturn = c.object + " = type {i32, ";
        sReturn = sReturn + "[" + c.size + " x i32], ";
        sReturn = sReturn + "[" + c.size + " x i1], ";
        sReturn = sReturn + "[" + c.size + " x double]}";
        return sReturn + NEW_LINE;
    }

    // declare %object* @calloc(i32, i32)
    // %cells = call %object* @calloc(i32 120, i32 3)
    public String visit(Calloc c) {
        String sReturn;
        if(c.var != null) {
            sReturn = c.var + " = call " + c.object + "* @calloc(i32 " + c.nitems + ", i32 " + c.size + ")";
        } else {
            sReturn = "declare " + c.object + "* @calloc(i32, i32)";
        }
        return sReturn + NEW_LINE;
    }

    // declare void @free(%object*)
    // call void @free(%object* %cells)
    public String visit(Free c) {
        String sReturn;
        if(c.var != null) {
            sReturn = "call void @free(" + c.object + "* " + c.var + ")";
        } else {
            sReturn = "declare void @free(" + c.object + "*)";
        }
        return sReturn + NEW_LINE;
    }

    // %p0 = getelementptr %object , %object* %cells , i32 0 , i32 1 , i32 7
    public String visit(ArrElm c) {
        String sReturn = c.immed + " = getelementptr ";
        sReturn = sReturn + c.object + " , " + c.object + "* " + c.var + " , i32 0 , i32 ";
        sReturn = sReturn + getCode(c.type) + " , i32 " + c.index;
        return sReturn + NEW_LINE;
    }

    // %p0 = getelementptr %object , %object* %cells , i32 0 , i32 0
    public String visit(ArrLen c) {
        String sReturn = c.immed + " = getelementptr ";
        sReturn = sReturn + c.object + " , " + c.object + "* " + c.var + " , i32 0 , i32 0";
        return sReturn + NEW_LINE;
    }

    // %object = type {i32, [40 x i32], [40 x i1], [40 x double]}
    public int getCode(Type type) {
        if(getBasic(type) instanceof Doub) {
            return 3;
        } else if(getBasic(type) instanceof Bool) {
            return 2;
        } else {
            return 1;
        }
    }

    public String visit (StoreObj c) {
        String sReturn = "store ";
        sReturn = sReturn + c.object + " " + c.immed + " , " + c.object + "* ";
        sReturn = sReturn + c.var;
        return sReturn + NEW_LINE;
    }

    public String visit (LoadObj c) {
        String sReturn = c.immed + " = load ";
        sReturn = sReturn + c.object + " , " + c.object + "* ";
        sReturn = sReturn + c.var;
        return sReturn + NEW_LINE;
    }

    public String visit (ReturnObj c) {
        String sReturn = "ret ";
        sReturn = sReturn + c.object + "* " + c.immed;
        return sReturn + NEW_LINE;
    }

    public BasicType getBasic(Type c) {
        return PrimitiveFunctions.extractBasic(c);
    }
}